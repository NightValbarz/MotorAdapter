package application;

import workflow.Automobile;
import workflow.CommonElectricMotor;
import workflow.CommonMotor;
import workflow.EconomicMotor;
import workflow.HybridMotor;
import workflow.MotorToElectricAdapter;

public class TemplateApp {

	public static void main(String[] args) {
		System.out.println("TEST OF VEHICLE COMMON MOTOR\n");
		Automobile auto1 = new Automobile(new CommonMotor());
		auto1.automaticMode();
		System.out.println();
		
		System.out.println("TEST ECONOMIC MOTOR\n");
		Automobile auto2 = new Automobile(new EconomicMotor());
		auto2.automaticMode();
		System.out.println();
		
		System.out.println("TESTING ADAPTER OF ELECTRIC MOTOR\n");
		Automobile auto3 = new Automobile(new MotorToElectricAdapter(new CommonElectricMotor()));
		auto3.automaticMode();
		System.out.println();
		
		System.out.println("TESTING ADAPTER OF HYBRIDMOTOR\n");
		Automobile auto4 = new Automobile(new HybridMotor(new CommonMotor() ,new CommonElectricMotor()));
		auto4.automaticMode();
	}

}
