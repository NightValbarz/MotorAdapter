package workflow;

public class EconomicMotor extends Template {

	@Override
	public Boolean speedUp() {
		System.out.println("Economic motor speeding Up!");
		return true;
	}

	@Override
	public Boolean turnOn() {
		System.out.println("Economic motor starting up!");
		return true;
	}

	@Override
	public Boolean turnOff() {
		System.out.println("Economic motor turning off");
		return true;
	}

}
