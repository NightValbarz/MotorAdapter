package workflow;

public class MotorToElectricAdapter extends Template {

	private ElectricMotor electricMotor;
	
	public MotorToElectricAdapter(ElectricMotor electricMotor) {
		this.electricMotor = electricMotor;
	}
	
	@Override
	public Boolean speedUp() {
		electricMotor.moveFaster();
		return true;
	}

	@Override
	public Boolean turnOn() {
		System.out.println("Electric motor adapter starting up");
		electricMotor.connect();
		electricMotor.activate();
		return true;
	}

	@Override
	public Boolean turnOff() {
		System.out.println("Electric motor adapter shutting down");
		electricMotor.stop();
		electricMotor.disconnect();
		return true;
	}

}
