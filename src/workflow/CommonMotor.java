package workflow;

public class CommonMotor extends Template {

	@Override
	public Boolean speedUp() {
		System.out.println("Common motor speeding up!");
		return true;
	}

	@Override
	public Boolean turnOn() {
		System.out.println("Common motor starting up!");
		return true;
	}

	@Override
	public Boolean turnOff() {
		System.out.println("Common motor turning off");
		return true;
	}

}
