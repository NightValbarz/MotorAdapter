package workflow;

public abstract class Template implements Motor {

	public void travel() {
		concreteMethod();
		turnOn();
		speedUp();
		turnOff();
		hook();
	}
	
	protected void concreteMethod() {
		System.out.println("Hi, i'm the template default message!");
	}
	
	protected void hook() {}

}
