package workflow;

public interface Motor {

	public Boolean speedUp();
	public Boolean turnOn();
	public Boolean turnOff();
	public void travel();
}
