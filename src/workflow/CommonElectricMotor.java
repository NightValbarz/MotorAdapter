package workflow;

public class CommonElectricMotor implements ElectricMotor {

	@Override
	public void activate() {
		System.out.println("ElectricMotor Activated beep");

	}

	@Override
	public void connect() {
		System.out.println("ElectricMotor Connected beep");

	}

	@Override
	public void disconnect() {
		System.out.println("ElectricMotor disconnected beep");

	}

	@Override
	public void moveFaster() {
		System.out.println("ElectricMotor speeding up, beep");

	}

	@Override
	public void stop() {
		System.out.println("ElectricMotor stopping");

	}

}
