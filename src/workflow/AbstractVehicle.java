package workflow;

public abstract class AbstractVehicle {

	private Motor motor;
	
	public AbstractVehicle(Motor motor) {
		this.motor = motor;
	}
	
	public Boolean keyIn() {
		System.out.println("Key has been put in, time turn on the engine!");
		motor.turnOn();
		return true;
	}
	
	public Boolean keyOut() {
		System.out.println("Key has been taken out, turning off the engine!");
		motor.turnOff();
		return true;
	}
	
	public Boolean throttle() {
		System.out.println("Driver wants to go faster, accelerating engine!");
		motor.speedUp();
		return true;
	}
	
	public void automaticMode() {
		motor.travel();
	}
}
