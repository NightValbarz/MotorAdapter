package workflow;

public interface ElectricMotor {

	public void activate();
	public void connect();
	public void disconnect();
	public void moveFaster();
	public void stop();
}
