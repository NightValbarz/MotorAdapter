package workflow;

public class HybridMotor extends Template {

	private Motor motor;
	private ElectricMotor electricMotor;
	
	public HybridMotor(Motor motor, ElectricMotor electricMotor) {
		this.motor = motor;
		this.electricMotor = electricMotor;
	}
	
	@Override
	public Boolean speedUp() {
		motor.speedUp();
		return true;
	}

	@Override
	public Boolean turnOn() {
		electricMotor.connect();
		electricMotor.activate();
		System.out.println("Beep electric motor out of battery, using gas motor");
		motor.turnOn();
		return true;
	}

	@Override
	public Boolean turnOff() {
		System.out.println("Turning off both motors");
		motor.turnOff();
		electricMotor.disconnect();
		return true;
	}

}
